import { expect } from 'chai'
import x from './index'

describe('semantic-test-one', function() {
  describe('all', function() {
    it('should be an array of strings', function() {
      expect(x.all).to.satisfy(isArrayOfStrings)

      function isArrayOfStrings(array) {
        return array.every(function(item) {
          return typeof item === 'string'
        })
      }
    })

    it('should contain `Vignesh Prasad`', function() {
      expect(x.all).to.include('Vignesh Prasad')
    })
  })

  describe('random', function() {
    it('should return a random item from x.all', function() {
      expect(x.all).to.include(x.random())
    })

    it('should return an array if a number is passed', function() {
      expect(x.random(5)).to.have.length(5)
    })

    it('should return a random item if any other argument type is passed', function() {
      expect(x.all).to.include(x.random('hahaha'))
    })
  })
})
