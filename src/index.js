import uniqueRandom from 'unique-random-array'
import starWarsNames from './starwars-names.json'

const getRandom = uniqueRandom(starWarsNames);

function random(number) {
  if(number === undefined) {
    return getRandom()
  } else if(typeof number === 'number') {
    const randomItems = new Array()
    while(randomItems.length < number) {
      randomItems.push(getRandom())
    }
    return randomItems
  } else {
    console.log('hmmmmmmmm')
    return getRandom()
  }
}

export default {
  all: starWarsNames,
  random
}
