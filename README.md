# semantic-test-one

[![codecov](https://codecov.io/gl/ViGi-P/semantic-test-one/branch/master/graph/badge.svg?token=ZUnQNNzPBE)](https://codecov.io/gl/ViGi-P/semantic-test-one)
[![version](https://img.shields.io/npm/v/semantic-test-one.svg)](https://www.npmjs.com/package/semantic-test-one)
[![downloads](https://img.shields.io/npm/dt/semantic-test-one)](https://www.npmjs.com/package/semantic-test-one)
[![Apache-2.0 license](https://img.shields.io/npm/l/semantic-test-one)](https://gitlab.com/ViGi-P/semantic-test-one/raw/master/LICENSE)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
