import { join } from 'path'

console.log('include', join(__dirname, 'src'))

export default (env, argv) => ({
  mode: argv.mode || 'development',
  entry: './src/index.js',
  output: {
    path: join(__dirname, 'dist'),
    libraryTarget: argv.mode === 'production' ? 'window' : 'umd',
    library: 'semteso',
    filename: argv.mode === 'production' ? 'browser.min.js' : 'index.umd.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: 'babel-loader'
      },
    ]
  }
})
